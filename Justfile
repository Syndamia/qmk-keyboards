# --- Keyboards --- #

chak: (flash-split "crkbd" "chak" "liatris" "rp2040_ce")

# --- Logic --- #

qmk := "qmk --config-file .config/qmk.ini"

clean:
	rm *.uf2 ./qmk_firmware/*.uf2 ./qmk_firmware/.build

flash-split keyboard keymap l_controller r_controller: (flash keyboard keymap l_controller)
	@echo "!! Wait for controller to reset (display/led flash) and plug other half in"
	@sleep 3
	just flash {{keyboard}} {{keymap}} {{r_controller}}

flash keyboard keymap controller: (compile keyboard keymap controller) (_flash keyboard keymap controller)

compile keyboard keymap controller:
	{{qmk}} compile -kb {{keyboard}} -km {{keymap}} -e CONVERT_TO={{controller}}

_flash keyboard keymap controller:
	@echo "!!"
	@echo "!! Put keyboard into flash mode"
	@echo "!!"
	@echo -en "Waiting..."
	@until [ -b /dev/disk/by-label/RPI-RP2 ]; do sleep 0.5; echo -en "."; done
	@echo
	udisksctl mount -b /dev/disk/by-label/RPI-RP2 || true
	cp ./{{keyboard}}*{{keymap}}*{{controller}}.uf2 $(lsblk /dev/disk/by-label/RPI-RP2 -o MOUNTPOINT -nr)

_configure:
	{{qmk}} config user.qmk_home=./qmk_firmware
	{{qmk}} config user.overlay_dir=.
	{{qmk}} doctor
