{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    qmk
    just
    util-linux
    udisks
  ];

  shellHook = ''
    qmk() { command qmk --config-file .config/qmk.ini $@; }
  '';
}
