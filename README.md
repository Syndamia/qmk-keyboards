# qmk-keyboards

Configurations for my QMK-based mechanical keyboards

chak - [corne choc](https://mechboards.co.uk/collections/kits/products/helidox-corne-kit?variant=40391708147917), [Kalih Choc Red (low-profile v1)](https://www.kailhswitch.com/mechanical-keyboard-switches/low-profile-key-switches/linear-mechanical-keyboard-switches.html), pro-micro rp2040, oled screens
: **Everything** purchased with **assembly** from [mechboards](https://mechboards.co.uk/)
