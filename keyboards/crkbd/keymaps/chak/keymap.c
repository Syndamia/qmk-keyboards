/*
Copyright 2019 @foostan
Copyright 2020 Drashna Jaelre <@drashna>
Copyright 2024 Kamen Mladenov <@syndamia>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Helpers */

#include QMK_KEYBOARD_H

/* Layer definitions */

enum layers {
	_QWERTY,
	_NUMBERS,
	_BRACES,
	_ARROWS,
	_FN,
	_ADJUST,
};

// Layer navigaton
const uint16_t
	L_NUMBS = MO(_NUMBERS),
	L_BRACS = MO(_BRACES),
	L_ADJST = MO(_ADJUST),

	SPC_ARR = LT(_ARROWS, KC_SPC),
	META_FN = LT(_FN, KC_RGUI);

/* Custom modifiers */

const uint16_t
	C_TAB  = LALT_T(KC_TAB),
	C_BSPC = LALT_T(KC_BSPC),
	C_ESC  = LSFT_T(KC_ESC),
	C_QUOT = LSFT_T(KC_QUOT),
	C_MINS = LCTL_T(KC_MINS),
	C_EQL  = LCTL_T(KC_EQL);

/* Key override definitions */

const key_override_t
	shift_super_system_sleep = ko_make_basic(MOD_MASK_SHIFT, KC_LGUI, KC_SLEP),
	shift_tab_underscore     = ko_make_basic(MOD_MASK_SHIFT,  KC_TAB, KC_UNDS)
	;
const key_override_t *key_overrides[] = {
	&shift_super_system_sleep,
	&shift_tab_underscore,
};

bool get_retro_tapping(uint16_t keycode, keyrecord_t *record) {
	switch (keycode) {
		case SPC_ARR: case C_TAB: case C_ESC: case C_BSPC:
			return false;
		default:
			return true;
	}
}

/* Layer definitions */

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  //Concatenated characters/keys means first is acessed with Shift, second without
  //Second line of key is for button when held down

    [_QWERTY] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
  //|  Tab   |   Q    |   W    |   E    |   R    |   T    |                    |   Y    |   U    |   I    |   O    |   P    |   ⟵    |
  //| L_Alt  |        |        |        |        |        |                    |        |        |        |        |        | L_Alt  |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|  Esc   |   A    |   S    |   D    |   F    |   G    |                    |   H    |   J    |   K    |   L    |   :;   |   "'   |
  //|L_Shift |        |        |        |        |        |                    |        |        |        |        |        |L_Shift |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|   -    |   Z    |   X    |   C    |   V    |   B    |                    |   N    |   M    |   <,   |   >.   |  _Tab  |   +=   |
  //|L_Cntrl |        |        |        |        |        |                    |        |        |        |        |        |L_Cntrl |
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
  //                                    | ☾L_Meta| NUMBERS| Space  |  |   ↲    | BRACES | R_Meta |
  //                                    |        |        | ARROWS |  |        |        |   FN   |
  //                                    `--------------------------'  `--------------------------'
        C_TAB,    KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,                         KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,  C_BSPC,
        C_ESC,    KC_A,    KC_S,    KC_D,    KC_F,    KC_G,                         KC_H,    KC_J,    KC_K,    KC_L, KC_SCLN,  C_QUOT,
       C_MINS,    KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,                         KC_N,    KC_M, KC_COMM,  KC_DOT,  KC_TAB,   C_EQL,
                                          KC_LGUI, L_NUMBS, SPC_ARR,     KC_ENT, L_BRACS, META_FN

  ),

    [_NUMBERS] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
  //|  Tab   |   !    |   @    |   #    |   $    |   %    |                    |   ^    |   &    |   *    |   |    |   ~    |   ⟵    |
  //| L_Alt  |        |        |        |        |        |                    |        |        |        |        |        |        |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|  Esc   |   1    |   2    |   3    |   4    |   5    |                    |   6    |   7    |   8    |   9    |   0    |   `    |
  //|L_Shift |        |        |        |        |        |                    |        |        |        |        |        |        |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|   -    |   ?    |        |        | PrScr  |        |                    |        |        |        |   >.   |        |        |
  //|L_Cntrl |        |        |        |        |        |                    |        |        |        |        |        |        |
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
  //                                    |        | ______ | Space  |  |   ↲    | BRACES |        |
  //                                    `--------------------------'  `--------------------------'
        C_TAB, KC_EXLM,   KC_AT, KC_HASH,  KC_DLR, KC_PERC,                      KC_CIRC, KC_AMPR, KC_ASTR, KC_PIPE, KC_TILD, KC_BSPC,
        C_ESC,    KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                         KC_6,    KC_7,    KC_8,    KC_9,    KC_0,  KC_GRV,
       C_MINS, KC_QUES, XXXXXXX, XXXXXXX, KC_PSCR, XXXXXXX,                      XXXXXXX, XXXXXXX, XXXXXXX,  KC_DOT, XXXXXXX, XXXXXXX,
                                          XXXXXXX, _______,  KC_SPC,     KC_ENT, L_BRACS, XXXXXXX
  ),

    [_BRACES] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
  //|        |        |        |  Del   | Insert |        |                    |        |  Home  |  End   |        |        |   ⟵    |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|  Esc   |   <    |   {    |   [    |   (    |   \    |                    |   /    |   )    |   ]    |   }    |   >    |        |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|        |        |        |        |        |        |                    |        |        |  Vol-  |  Vol+  |        |        |
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
  //                                    |        | NUMBERS| Space  |  |   ↲    | ______ |        |
  //                                    `--------------------------'  `--------------------------'
      XXXXXXX, XXXXXXX, XXXXXXX,  KC_DEL,  KC_INS, XXXXXXX,                      XXXXXXX, KC_HOME,  KC_END, XXXXXXX, XXXXXXX, KC_BSPC,
       KC_ESC,   KC_LT, KC_LCBR, KC_LBRC, KC_LPRN, KC_BSLS,                      KC_SLSH, KC_RPRN, KC_RBRC, KC_RCBR,   KC_GT, XXXXXXX,
      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                      XXXXXXX, XXXXXXX, KC_VOLD, KC_VOLU, XXXXXXX, XXXXXXX,
                                          XXXXXXX, L_NUMBS,  KC_SPC,     KC_ENT, _______, XXXXXXX
  ),

    [_ARROWS] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
  //|   ~`   |        |        |        |        |        |                    |        |        |        |        |        |   ⟵    |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|  Esc   |        |        |        |        |        |                    |   ←    |   ↓    |   ↑    |   →    |   {[   |   }]   |
  //|L_Shift |        |        |        |        |        |                    |        |        |        |        |        |        |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|        |        |        |        |        |        |                    |        |        |        |        |        |   |\   |
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
  //                                    |        |        | ______ |  | Enter  |        |        |
  //                                    `--------------------------'  `--------------------------'
       KC_GRV, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_BSPC,
        C_ESC, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                      KC_LEFT, KC_DOWN,   KC_UP, KC_RGHT, KC_LBRC, KC_RBRC,
      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_BSLS,
                                          XXXXXXX, XXXXXXX, _______,     KC_ENT, XXXXXXX, XXXXXXX
  ),

    [_FN] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
  //|        |        |        |        |        |        |                    |        |        |        |        |        |        |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|        |   F1   |   F2   |   F3   |   F4   |   F5   |                    |   F6   |   F7   |   F8   |   F9   |  F10   |        |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|        |  F11   |  F12   |  F13   |  F14   |  F15   |                    |  F16   |  F17   |  F18   |  F19   |  F20   |        |
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
  //                                    | ADJUST |        |        |  |        |        | ______ |
  //                                    `--------------------------'  `--------------------------'
      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
      XXXXXXX,   KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,                        KC_F6,   KC_F7,   KC_F8,   KC_F9,  KC_F10, XXXXXXX,
      XXXXXXX,  KC_F11,  KC_F12,  KC_F13,  KC_F14,  KC_F15,                       KC_F16,  KC_F17,  KC_F18,  KC_F19,  KC_F20, XXXXXXX,
                                          L_ADJST, XXXXXXX, XXXXXXX,    XXXXXXX, XXXXXXX, _______
  ),

    [_ADJUST] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
  //|        |        |        |        |  _RBT  |        |                    |        |        |        |        |        |        |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|        |        |        | _DBG   |        |        |                    | _HSWAP |        |        | _BLTOG |        |        |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|        |        |        |        |        | _BOOT  |                    |        |        |        |        |        |        |
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
  //                                    | ______ |        |        |  |        |        | ______ |
  //                                    `--------------------------'  `--------------------------'
      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,  QK_RBT, XXXXXXX,                      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
      XXXXXXX, XXXXXXX, XXXXXXX, DB_TOGG, XXXXXXX, XXXXXXX,                      SH_TOGG, XXXXXXX, XXXXXXX, BL_TOGG, XXXXXXX, XXXXXXX,
      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, QK_BOOT,                      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
                                          _______, XXXXXXX, XXXXXXX,    XXXXXXX, XXXXXXX, _______
  )
};

// Template
//  [] = LAYOUT_split_3x6_3(
  //,-----------------------------------------------------.                    ,-----------------------------------------------------.
  //|        |        |        |        |        |        |                    |        |        |        |        |        |        |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|        |        |        |        |        |        |                    |        |        |        |        |        |        |
  //|--------+--------+--------+--------+--------+--------|                    |--------+--------+--------+--------+--------+--------|
  //|        |        |        |        |        |        |                    |        |        |        |        |        |        |
  //|--------+--------+--------+--------+--------+--------+--------|  |--------+--------+--------+--------+--------+--------+--------|
  //                                    |        |        |        |  |        |        |        |
  //                                    `--------------------------'  `--------------------------'
//    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
//    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
//    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                      XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
//                                        XXXXXXX, XXXXXXX, XXXXXXX,    XXXXXXX, XXXXXXX, XXXXXXX
//),

#ifdef CONVERT_TO_LIATRIS
void keyboard_pre_init_user(void) {
  // Set our LED pin as output
  setPinOutput(24);
  // Turn the LED off
  // (Due to technical reasons, high is off and low is on)
  writePinHigh(24);
}
#endif
